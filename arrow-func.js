/*
 * Arrow funtions: is a feature introduced in ES6
 * Is a shorter function syntax
 *
 *
*/

const dragonEvents = [

    {type:'attack', value:12, target:'player-one'}, 
    {type:'attack', value:23, target:'player-two'},
    {type:'attack', value:12, target:'player-one'},
    {type:'yawn', value:40},
    {type:'eat', target:'horse'}
]

const totalDamageOnPlayer = dragonEvents
    .filter(function (event) {
        return event.type === 'attack'
    }) 
    .filter(function (event) {
        return event.type === 'attack'
    })    
    .filter(function (event) {
        return event.target === 'player-one'
    })
    .map(function (event) {
        return event.value
    })
    .reduce(function (prev, value) {
        return (prev || 0) + value 
    })

const totalDamageOnPlayerArrowStyle = dragonEvents
    .filter( event => event.type === 'attack') 
    .filter( event => event.type === 'attack')    
    .filter( event => event.target === 'player-one')
    .map( event =>  event.value)
    .reduce((prev, value) => (prev || 0) + value)


console.log ('Total damage for player one:\n', totalDamageOnPlayer) // normal functions
console.log ('Total damage for player one:\n', totalDamageOnPlayerArrowStyle) //arrow functions
 
