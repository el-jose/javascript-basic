"use strict";
/*
 *PROTOTYPES
 * ALL javascript objects inherit properties and methods from a prototype.
 * the object.prototype is on the top of the proto inheritance chain.
 * sometimes you need to add new properties and methods to all existing objects of a given type.
 * sometimes you need to add new properties or methods to an object constructor.
 * Prototype is the way to do it.
*/

function Particle() {
    this.x = 100;
    this.y = 50;
};

let p1 = new Particle ();
let p2 = new Particle ();

Particle.prototype.double = function () {
    this.x*=2;
    this.y*=2;	
};

console.log (p1);
console.log (p2);
p1.double();
console.log (p1);
console.log (p2);
