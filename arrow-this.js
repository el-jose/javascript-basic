/*
 * Arrow functions usages:
 * in this case i'm defining two class atributes: count and wait
 * if i try to use setInterval using traditional function notation (see the block commented) 
 * will be fail, because THIS.count is not defined in the scope of countIt function.
 * using the arrow notation, the context come from start(), the class context. in that context
 * this.count and this.wait are defined.
 *
*/

function setup() {
    const counter1 = new Counter(100, 500);
    counter1.start();
}

class Counter {
    constructor(start, wait) {
        this.count = start;
	this.wait = wait;	    
    }
    
    /*
    start() {
        setInterval(function countIt() {
	    console.log (this.count);
	    this.count++;
	}, this.wait);
    }

    */

    start() {
        setInterval(() => {
	    this.countIt(); 
	}, this.wait);
    }	
	    
    countIt() {
        console.log(this.count);
        this.count++;
    }

}

setup();
