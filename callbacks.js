"use strict";

/* CALLBACKS
 *
 * A  function passed as argument is called callback function.
 * callbacks are useful to make sure that certain code doesn't execute
 * until other code has already finished.
 *
 * In this example getPosts is passed as callback to createPost(),
 * that means once createPost finished execution  getPosts is gonna be executed.
 *
 *
*/

const posts = [
    {title: "post one", body: "this is post one"},
    {title: "post two", body: "this is post two"}
];


function getPosts () {
    setTimeout (() => {
        let output = '';
	    posts.forEach((post, index) => {
	        output += `${post.title}\n`;
	    });
	console.log (output)
    }, 1000);
};

function createPost (post, callback) {
    setTimeout
};


console.log (getPosts())
