/*

CLOSURES: Are functions with preserved data

*/
var util = require("util");
var passed = 4;
var addTo = function () {
    var inner = 2;
    return passed + inner;
};

console.log (util.inspect(addTo, { showHidden: true, depth: 6 }));
